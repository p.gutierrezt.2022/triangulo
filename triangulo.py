#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    valor = str(number)
    return valor * number

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    if number>9:
        raise ValueError
    text= ''
    for x in range(1, number+1):
        resultado=line(x)
        print(resultado)
    return ""

def main():
    try:
        number: int = sys.argv[1]
        text = triangle(int(number))
        print(text)
    except ValueError:
        number: int = 9
        text = triangle(int(number))
        print(text)
if __name__ == '__main__':
    main()
